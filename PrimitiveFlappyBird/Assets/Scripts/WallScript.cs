﻿using UnityEngine;
using System.Collections;

public class WallScript : MonoBehaviour {
    private bool scored;

    void Start()
    {
        scored = false;
    }
    void Update()
    {
        //barrier is being destroyed after reaching some point. Faster way of doing this than by colliders.
        if (transform.position.x < -40) Destroy(gameObject); 
        //If player has reached middle of this barrier score is added.
        if (transform.position.x <= GameObject.FindGameObjectWithTag("Player").transform.position.x && scored == false)
        {
            scored = true;
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().score++;
            GetComponent<AudioSource>().Play();
        }
    }
}
