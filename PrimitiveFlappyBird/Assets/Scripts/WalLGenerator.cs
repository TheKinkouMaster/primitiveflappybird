﻿using UnityEngine;
using System.Collections;

public class WalLGenerator : MonoBehaviour {
    public bool isActive;

    public int freeSpaceSize;
    public int maxBarriers;
    public float delay;
    public float timer;

    public GameObject barrierPrefab;

    void Start()
    {
        timer = 0;
    }

    void Update()
    {
        if(isActive)
        {
            timer += Time.deltaTime;
            GameObject[] numberOfBarriers = GameObject.FindGameObjectsWithTag("Barrier");
            if(numberOfBarriers.Length < maxBarriers && timer >= delay)
            {
                timer -= delay;
                CreateBarrier();
            }
        }
    }

    public void SetActive(bool value)
    {
        isActive = value;
    }
    void CreateBarrier()
    {
        //picking spot for lower barrier. Always 1 unit from bottom edge.
        float randRange = 48 - freeSpaceSize;
        float spotToResp = (Random.Range(0, randRange));
        spotToResp -= 24;

        //creating barrier -> setting up lower one -> setting up upper one -> adjusting size so they touch edges
        GameObject barrier = (GameObject)Instantiate(barrierPrefab, new Vector3(40, spotToResp - (freeSpaceSize / 2), 0), transform.rotation);
        barrier.transform.Find("BotBarrier").transform.position = new Vector3(40, spotToResp, 0);
        barrier.transform.Find("TopBarrier").transform.position = new Vector3(40, spotToResp + freeSpaceSize, 0);

        barrier.transform.Find("BotBarrier").transform.localScale = new Vector3(1, 25 + spotToResp, 1);
        barrier.transform.Find("TopBarrier").transform.localScale = new Vector3(1, 50 - ((spotToResp+25) + freeSpaceSize) , 1);

        barrier.GetComponent<Rigidbody>().velocity = new Vector3(-10, 0, 0);
    }
}
