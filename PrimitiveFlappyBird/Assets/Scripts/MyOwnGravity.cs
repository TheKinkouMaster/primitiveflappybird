﻿using UnityEngine;
using System.Collections;

public class MyOwnGravity : MonoBehaviour {
    public bool working;
    void Start()
    {
        working = false;
    }
    void Update()
    {
        if (working == true)
        {
            GetComponent<Rigidbody>().AddForce(0, -50, 0);
        }
    }
}
