﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {
    public Transform bird;
    public bool startGame;

    void Start()
    {
        startGame = true;
        GetComponent<MyOwnGravity>().working = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(startGame == true)
            {
                GetComponent<MyOwnGravity>().working = true;
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().InitBarriers();
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().EnableUI("Ingame");
            }
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 1500, 0));
        }

        Rotate();
    }
    void OnCollisionEnter(Collision collision)
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().GameLost();
    }
    void OnTriggerEnter()
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().score++;
    }
    void Rotate()
    {
        float velocity = GetComponent<Rigidbody>().velocity.y;
        float rotation = (-0.009f * velocity * velocity) + (velocity * 1.8f); //usually bird reaches speed lower that 100. y(100) = 90; y(0) = 0;
        bird.rotation = Quaternion.Euler(0, 0, rotation);
    }
}

