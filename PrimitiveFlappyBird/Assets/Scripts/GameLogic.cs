﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameLogic : MonoBehaviour {
    public Text       floatingText;
    public GameObject menuUI;
    public GameObject prepareUI;
    public GameObject gameUI;
    public GameObject lostUI;

    public int score;


    public GameObject birdPrefab;

    void Start()
    {
        EnableUI("Main");
        score = 0;
    }
    void Update()
    {
        if(true)
        {
            gameUI.GetComponent<Text>().text = "Score: " + score;
        }
        if (true)
        {
            lostUI.GetComponent<Text>().text = "Your score was: " + score;
        }
    }

    //Changes UI after receiving command
    public void EnableUI(string menu)
    {
        if (menu == "Main")
        {
            menuUI.SetActive(true);
            prepareUI.SetActive(false);
            gameUI.SetActive(false);
            lostUI.SetActive(false);
        }
        if (menu == "Prepare")
        {
            menuUI.SetActive(false);
            prepareUI.SetActive(true);
            gameUI.SetActive(false);
            lostUI.SetActive(false);
        }
        if (menu == "Ingame")
        {
            menuUI.SetActive(false);
            prepareUI.SetActive(false);
            gameUI.SetActive(true);
            lostUI.SetActive(false);
        }
        if (menu == "Lost")
        {
            menuUI.SetActive(true);
            prepareUI.SetActive(false);
            gameUI.SetActive(false);
            lostUI.SetActive(true);
        }
    }
    public void EndGame()
    {
        GetComponent<WalLGenerator>().SetActive(false);
        Application.Quit();
    }
    public void StartGame()
    {
        score = 0;            //Score Reset
        EnableUI("Prepare");  //New UI
        InitPlayer();         //Creating Player
    }
    public void GameLost()
    {
        EnableUI("Lost");
        ClearMap();
        GetComponent<WalLGenerator>().SetActive(false);

        GetComponent<AudioSource>().Play();
    }

    public void InitPlayer()
    {
        Instantiate(birdPrefab, new Vector3(0, 0, 0), transform.rotation);
    }
    public void InitBarriers()
    {
        GetComponent<WalLGenerator>().SetActive(true);
    }
    public void ClearMap()
    {
        GameObject[] stuffToDelete = GameObject.FindGameObjectsWithTag("Barrier");
        foreach (GameObject barrier in stuffToDelete)
            Destroy(barrier);
        stuffToDelete = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject barrier in stuffToDelete)
            Destroy(barrier);
    }
    
}
